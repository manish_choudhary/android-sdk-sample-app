package sample.livquik.com.android_sdk_sample_app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.livquik.qwsdkui.QWSDKLibInit;
import com.livquik.qwsdkui.core.QWConstants;
import com.livquik.qwsdkui.model.QWParams;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getName();
    EditText mobileText;
    EditText signatureText;
    EditText partneredText;
    EditText amountText;
    EditText udf1;
    EditText udf2;
    EditText udf3;
    EditText udf4;

    QWParams qwParams;
    Context context;

    // UAT creds
    String amount = "1";
    //    String mobile = "7710999820";
//    String signature = "69e41f1b3c89e22eab9b28c1da107e512ffd4358977a40e0b4b16981b0d265cd";
//    String partnerid = "10";
    String mobile = "4215321451";
    String signature = "be09367680378d91fc6da0e1a219d8b0a2acd410d6e65b124f15908df8acd13e";
    String partnerid = "112";
    String tudf1 = "some:value1";
    String tudf2 = "skafjdl:asdf";
    String tudf3 = "kjk:kjadsf";
    String tudf4 = "asdf:sdaf";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null)
            getSupportActionBar().setTitle("Android-sdk-sample-app");

        signatureText = (EditText) findViewById(R.id.signature);
        mobileText = (EditText) findViewById(R.id.mobile);
        partneredText = (EditText) findViewById(R.id.partnerid);
        udf1 = (EditText) findViewById(R.id.udf1);
        udf2 = (EditText) findViewById(R.id.udf2);
        udf3 = (EditText) findViewById(R.id.udf3);
        udf4 = (EditText) findViewById(R.id.udf4);
        udf1.setText(tudf1);
        udf2.setText(tudf2);
        udf3.setText(tudf3);
        udf4.setText(tudf4);
        amountText = (EditText) findViewById(R.id.amount);
        qwParams = new QWParams();
        context = this;
        signatureText.setText(signature);
        mobileText.setText(mobile);
        partneredText.setText(partnerid);
        amountText.setText(amount);
        qwParams.setEnv(QWConstants.ENV_TEST);//Use UAT for testing
        Button payButton = (Button) findViewById(R.id.pay_button);
        Button showCards = (Button) findViewById(R.id.show_cards);

        payButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initqwparm();
                QWSDKLibInit.init(context, qwParams, QWConstants.PAYMENT);
            }
        });

        showCards.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                initqwparm();
                QWSDKLibInit.init(context, qwParams, QWConstants.SHOW_CARDS);
            }
        });
    }

    void initqwparm() {
        signature = signatureText.getText().toString();
        mobile = mobileText.getText().toString();
        partnerid = partneredText.getText().toString();
        amount = amountText.getText().toString();
        tudf1 = udf1.getText().toString();
        tudf2 = udf2.getText().toString();
        tudf3 = udf3.getText().toString();
        tudf4 = udf4.getText().toString();
        qwParams.setMobile(mobile);
        qwParams.setSignature(signature);
        qwParams.setPartnerid(partnerid);
        qwParams.setAmount(amount);
        qwParams.setUdf1(tudf1);
        qwParams.setUdf3(tudf3);
        qwParams.setUdf2(tudf2);
        qwParams.setUdf4(tudf4);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == QWConstants.QWSDKID) {
            if (data != null) {
                Log.d(TAG, data.getStringExtra(QWConstants.RESPONSE_DESC) + "");
                if (resultCode == Activity.RESULT_CANCELED) {
                    Toast.makeText(this, data.getStringExtra(QWConstants.RESPONSE_DESC) + "", Toast.LENGTH_SHORT).show();
                } else if (resultCode == QWConstants.PAYMENT_SUCCESS_CODE) {
                    String paymentid = data.getStringExtra(QWConstants.PAYMENTID);
                    String msg = data.getStringExtra(QWConstants.RESPONSE_DESC);
                    Log.d(TAG, "paymentid: " + paymentid + " msg :" + msg);
                    Toast.makeText(this, "paymentid " + paymentid + " msg " + msg + "", Toast.LENGTH_SHORT).show();
                } else if (resultCode == QWConstants.PAYMENT_FAILED_CODE) {
                    String paymentid = data.getStringExtra(QWConstants.PAYMENTID);
                    String msg = data.getStringExtra(QWConstants.RESPONSE_DESC);
                    Log.d(TAG, "paymentid: " + paymentid + " msg :" + msg);
                    Toast.makeText(this, "paymentid " + paymentid + " msg " + msg + "", Toast.LENGTH_SHORT).show();
                } else if (resultCode == QWConstants.NO_ACTION_PROVIDED_CODE) {
                    Log.d(TAG, "" + data.getStringExtra(QWConstants.RESPONSE_DESC) + "");
                    Toast.makeText(this, data.getStringExtra(QWConstants.RESPONSE_DESC) + "", Toast.LENGTH_SHORT).show();
                } else if (resultCode == QWConstants.INPUT_VALIDATION_FAILED) {
                    Log.d(TAG, data.getStringExtra(QWConstants.RESPONSE_DESC) + "");
                    Toast.makeText(this, data.getStringExtra(QWConstants.RESPONSE_DESC) + "", Toast.LENGTH_SHORT).show();
                }
            }

        }
    }
}
